<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false">
                <span class="sr-only">Navigatie open/sluit</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">
                <img src="{{ asset('images/sedun-logo-big.png') }}" class="logo-white" width="150">
                <img src="{{ asset('images/sedun-logo-dark.png') }}" class="logo-dark" width="125">
            </a>
        </div>

        <div class="collapse navbar-collapse" id="navbar">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="#web">Home</a>
                </li>
                <li>
                    <a href="#about">Over ons</a>
                </li>
                <li>
                    <a href="#how-it-works">Hoe werkt het?</a>
                </li>
                <li>
                    <a href="#contact">Contact</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
