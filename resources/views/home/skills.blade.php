<div class="container margin-top-xs-80">
    <div class="row skills">
        <div class="col-md-1">
            <i class="fa fa-4x fa-shield icon margin-top-xs-15"></i>
        </div>
        <div class="col-md-3">
            <h4 class="text-uppercase">Veiligheid</h4>
            <p>
                Tijdens de ontwikkeling van uw website letten we ten alle tijden op
                de veiligheid en efficentie. We streven naar een veilig internet.
            </p>
        </div>
        <div class="col-md-1">
            <i class="fa fa-4x fa-pencil-square-o icon margin-top-xs-15"></i>
        </div>
        <div class="col-md-3">
            <h4 class="text-uppercase">Design</h4>
            <p>
                Ieder design wordt vanaf het begin ontwikkeld gebaseerd op
                uw huisstijl. Een uniek design hoort herkenbaar te zijn.
            </p>
        </div>
        <div class="col-md-1">
            <i class="fa fa-4x fa-tablet icon margin-top-xs-15"></i>
        </div>
        <div class="col-md-3">
            <h4 class="text-uppercase">Responsive</h4>
            <p>
                Er bestaan veel apparaten met verschillende schermformaten. We
                zorgen dat uw website op ieder formaat beschikbaar is.
            </p>
        </div>
    </div>
</div>
