<div id="partners">
    <div class="col-md-6 text-center">
        <h1 class="text-uppercase margin-top-xs-55">Onze partners</h1>
        <ul class="list-inline margin-top-xs-30">
            <li><img class="img" src="{{ asset('images/partners/acosh.png') }}" alt="Acosh logo"></li>
            <li><img class="img" src="{{ asset('images/partners/glu.png') }}" alt="Glu logo"></li>
            <li><img class="img" src="{{ asset('images/partners/bureau.jpg') }}" alt="Bureau logo"></li>
        </ul>
        <p class="margin-top-xs-30">
            <small>Partner worden? Mail naar <a href="mailto:contact@acosh.nl">contact@sedun.nl</a></small>
        </p>
    </div>
</div>
