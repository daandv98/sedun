<header>
    <div class="container">
        <div class="row header-text">
            <div class="col-md-8 col-md-offset-2 text-center">
                <h1 class="text-uppercase">
                    Twee ambitieuze programmeurs
                </h1>
                <p>We stampen innovatieve websites uit de grond!</p>
                <a href="#about" class="btn btn-big btn-white">Leer ons kennen</a>
            </div>
        </div>
    </div>
</header>
