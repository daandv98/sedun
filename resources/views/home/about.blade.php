<div id="about" class="margin-top-xs-80">
    <div class="container">
        <div class="row margin-top-xs-50">
            <div class="col-md-6">
                <h2 class="text-uppercase">Over ons</h2>
                <p>
                    Met ruim 6 jaar ervaring op gebied van innovatie en ontwikkelingen omtrent websites, realiseren wij kwalitatieve unieke middel-grote websites. De markt blijft constant groeien en wij werken hier aan mee. We streven naar een groei aan klanten en bieden de beste service om van uw idee een mooie realisatie te ontwikkelen. We consumeren een gestoord aantal aan koffie om efficient, snel en gefocust te werken.
                    <br><br>
                    Ons team bestaat uit twee ambitieuze programmeurs. We delen onze kennis graag om zo efficient mogelijk te werken en breiden dagelijks onze kennis uit.
                </p>
            </div>
            <div class="col-md-5 col-md-offset-1 team">
                <div class="row margin-top-xs-50">
                    <div class="col-md-4">
                        <div class="daan margin-bottom-15">
                            <div class="profile">
                                <img class="img-circle" src="{{ asset('images/team/daan.png') }}" alt="...">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <h4 class="media-heading text-uppercase">Daan de Vos</h4>
                        Daan is onze Fullstack Developer, hij programmeert al vanaf zijn 13e leeftijd en heeft een eionderneming genaamd Acosh. Hij heeft o.a. ervaring opgedaan bij Foryard - een innovatielab.
                    </div>
                </div>
                <hr>
                <div class="row margin-top-xs-15">
                    <div class="col-md-4">
                        <div class="miriam margin-bottom-15">
                            <div class="profile">
                                <img class="img-circle" src="{{ asset('images/team/miriam.jpg') }}" alt="...">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <h4 class="media-heading text-uppercase">Miriam Kant</h4>
                        Miriam is onze Backend Developer die ervaring heeft opgedaan DMS - een bedrijf dat zich specialiseert in CMS en CRM-systemen. Ze programmeert nu al 2 jaar lang en beheerst PHP als een pro!
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
