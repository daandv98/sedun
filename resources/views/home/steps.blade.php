<div class="container margin-top-xs-80" id="how-it-works">
    <div class="row">
        <div class="col-md-1 margin-bottom-35">
            <i class="fa fa-4x fa-pencil" aria-hidden="true"></i>
        </div>
        <div class="col-md-5 margin-bottom-35">
            <h4 class="text-uppercase">Stap 1. <small>Neem contact op met het Grafisch Lyceum Utrecht</small></h4>
            <p>
                Meldt uw bedrijf aan bij het GLU als 'Bureau' bedrijf om een gratis website te laten maken door studenten. De studenten leren hoe het is om een klant te hebben met eisen.
            </p>
        </div>
        <div class="col-md-1 margin-bottom-35">
            <i class="fa fa-4x fa-building" aria-hidden="true"></i>
        </div>
        <div class="col-md-5 margin-bottom-35">
            <h4 class="text-uppercase">Stap 2. <small>Vraag naar Sedun als aannemer</small></h4>
            <p>
                Vraag naar Sedun als aannemer voor het laten maken van uw website. Met ons als voorkeur is uw zekerheid voor een succesvol resultaat groter dan gemiddeld.
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-1 margin-bottom-35">
            <i class="fa fa-4x fa-calendar-check-o" aria-hidden="true"></i>
        </div>
        <div class="col-md-5 margin-bottom-35">
            <h4 class="text-uppercase">Stap 3. <small>Plan een afspraak</small></h4>
            <p>
                Wacht op een telefoontje van ons! We bellen u op om een afspraak met u te maken zodra we klaar zijn om de uitdaging met u aan te gaan.
            </p>
        </div>
        <div class="col-md-1 margin-bottom-35">
            <i class="fa fa-4x fa-check-square-o" aria-hidden="true"></i>
        </div>
        <div class="col-md-5 margin-bottom-35">
            <h4 class="text-uppercase">Stap 4. <small>Bespreek uw gewenste eindresultaat</small></h4>
            <p>
                Laat weten wat u als gewenste eindresultaat wilt zien. Tijdens dit gesprek bespreken we met 'user stories' welke functionaliteiten in de website moeten komen.
            </p>
        </div>
    </div>
    <div class="text-center">
        <small class="text-muted">Liever volledige zekerheid? Huur dan <a href="https://acosh.nl/?ref=sedun">Acosh</a> in voor uw website.</small>
    </div>
</div>
