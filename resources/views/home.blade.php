@extends('layouts.web')

@section('content')
    @include('shared.navbar')
    @include('home.header')
    @include('home.skills')
    @include('home.about')
    @include('home.steps')
    <div class="background-lightest-green contact margin-top-xs-80">
        <div class="container">
            <div class="row margin-top-xs-45 margin-bottom-80">
                @include('home.contact')
                @include('home.partners')
            </div>
        </div>
        @include('home.footer')
    </div>
@endsection
